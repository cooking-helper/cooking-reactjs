export const MEAL_PLANNING = [
    {
        date: "2019-14-07",
        day_of_week: "Sunday",
        meals: null
    },
    {
        date: "2019-15-07",
        day_of_week: "Monday",
        meals: null
    },
    {
        date: "2019-16-07",
        day_of_week: "Tuesday",
        meals: null
    },
    {
        date: "2019-17-07",
        day_of_week: "Wednesday",
        meals: null
    },
    {
        date: "2019-18-07",
        day_of_week: "Thursday",
        meals: {
            "LUNCH": [1],
        }
    },
    {
        date: "2019-19-07",
        day_of_week: "Friday",
        meals: {
            "BREAKFAST": [1],
            "DINNER": [2, 3],
        }
    },
    {
        date: "2019-20-07",
        day_of_week: "Saturday",
        meals: null
    }
]