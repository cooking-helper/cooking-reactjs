import { createStore } from 'redux'
import { MealPlanningReducer } from './MealPlanningReducer'
import { reducer as formReducer } from 'redux-form'
import { combineReducers } from 'redux'

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            meal_planning: MealPlanningReducer,
            form: formReducer
        })
    );
    return store;
};