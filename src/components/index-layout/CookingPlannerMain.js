import React from "react";
import { Switch, Route } from 'react-router-dom';
import MealPlanner from "../meal-planner/MealPlanner";
import RecipesDB from "../recipes-db/RecipesDB";
import RecipesSearch from "../recipes-search/RecipesSearch";
export default class CookingPlannerLayout extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Switch>
                <Route exact path='/' component={MealPlanner}></Route>
                <Route exact path='/recipes-db' component={RecipesDB}></Route>
                <Route exact path='/recipes-search' component={RecipesSearch}></Route>
            </Switch>
        );
    }
}