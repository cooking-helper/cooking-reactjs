import React from "react";
import CookingPlannerMenu from "./CookingPlannerMenu";
import CookingPlannerMain from "./CookingPlannerMain";

export default class CookingPlannerLayout extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="cooking-planner">
        <div>
          <CookingPlannerMenu />
          <CookingPlannerMain />
        </div>
      </div>
    );
  }
}
