import React from "react";
import { NavLink } from 'react-router-dom';
import logo from './cooking_planner.svg';
import './CookingPlannerMenu.css';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem
} from 'react-bootstrap';
export default class CookingPlannerMenu extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Navbar className="white-nav-bar" variant="light">
                <Navbar.Brand href="/">
                    <img
                        src={logo}
                        width="100"
                        height="60"
                        className="d-inline-block align-top"
                        alt="Cooking Planner logo"
                    /></Navbar.Brand>
                <Nav className="mr-auto">
                    <NavItem>
                        <NavLink className="nav-link" exact activeClassName="active" to="/">Meal Planner</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className="nav-link" exact activeClassName="active" to="/recipes-db">Recipes DB</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className="nav-link" exact activeClassName="active" to="/recipes-search">Recipes Search</NavLink>
                    </NavItem>
                </Nav>
            </Navbar>);
        /* 
        <Form inline>
             <FormControl type="text" placeholder="Search" className="mr-sm-2" />
             <Button variant="outline-success">Search</Button>
         </Form>
         */
    }
}