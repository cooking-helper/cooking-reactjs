import './MealPlannerNavBar.css';
import React from "react";
import {
    Toolbar,
    Link
} from '@material-ui/core'

import Moment from 'react-moment';
export default class MealPlanner extends React.Component {

    constructor(props) {
        super(props);
        this.handleNext = props.handleNext;
        this.handlePrevious = props.handlePrevious;
    }

    render() {
        const firstWeekDay = this.props.start_date;
        const lastWeekDay = this.props.end_date;
        const previousText = '< Previous';
        const nextText = 'Next >';
        return (
            <Toolbar>
                <div className="previous">
                    <Link component="button" onClick={() => this.handlePrevious()}>{previousText}</Link>
                </div>
                <div className="mx-auto">
                    <span className="week-title"> <Moment format="YYYY">{lastWeekDay}</Moment> - Week  <Moment format="w">{firstWeekDay}</Moment></span>
                    <span className="start-date-title"><Moment format="LL">{firstWeekDay}</Moment></span>
                    <span className="date-separator">-</span>
                    <span className="end-date-title"><Moment format="LL">{lastWeekDay}</Moment></span>
                </div>
                <div className="next">
                    <Link component="button" onClick={() => this.handleNext()}>{nextText}</Link>
                </div>
            </Toolbar>
        );
    }
}