import { Grid, Checkbox, FormControlLabel, FormGroup, Button, FormLabel, Input } from '@material-ui/core';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import { reduxForm, Field } from 'redux-form'


const renderDatePicker = ({ input, label, meta: { touched, error }, children, ...custom }) => (
    <MuiPickersUtilsProvider utils={DateFnsUtils} required>
        <Grid container>
            <DatePicker
                margin="normal"
                label={label}
                onChange={input.onChange}
                value={input.value}
            />
        </Grid>
    </MuiPickersUtilsProvider>
)

const renderCheckbox = ({ input, label }) => (
    <FormControlLabel
        control={<Checkbox
            checked={input.value ? true : false}
            onChange={input.onChange}
        />}
        label={label}>
    </FormControlLabel >
)

const MealGeneratorForm = props => {
    const { handleSubmit, handleCancel, start_date } = props
    return (
        <form onSubmit={handleSubmit}>
            {JSON.stringify(start_date)}
            <FormGroup row>
                <Field name="start_date" component={renderDatePicker} label="Start date" />
            </FormGroup>
            <FormGroup row>
                <Field name="end_date" component={renderDatePicker} label="End date" />
            </FormGroup>
            <FormGroup row>
                <FormLabel component="legend">Meals</FormLabel>
                <Field name="breakfast" component={renderCheckbox} label="Breakfast" />
                <Field name="lunch" component={renderCheckbox} label="Lunch" />
                <Field name="dinner" component={renderCheckbox} label="Dinner" />
            </FormGroup>
            <div>
                <Button type="submit" color="primary">
                    Generate
                </Button>
                <Button color="primary" onClick={handleCancel}>
                    Cancel
                </Button>
            </div>
        </form>
    )
}

export default reduxForm({
    form: 'MealGeneratorForm', // a unique identifier for this form
})(MealGeneratorForm);

/*  render() {
      const { selectedStartDate, selectedEndDate } = this.state;
      return (
          <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
              <Row className="form-group">
                  <Field name="start_date" component={renderDatePicker} label="Start date" />
                  <MuiPickersUtilsProvider utils={DateFnsUtils} required>
                      <Grid container justify="space-around">
                          <DatePicker
                              margin="normal"
                              label="Start date"
                              value={selectedStartDate}
                              onChange={this.handleStartDateChange}
                              emptylabel="Select date"
                          />
                      </Grid>
                  </MuiPickersUtilsProvider>
              </Row>
              <Row className="form-group">
                  <Field name="employed" component={renderCheckbox} label="Employed" />
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <Grid container justify="space-around">
                          <DatePicker
                              margin="normal"
                              label="End date"
                              value={selectedEndDate}
                              onChange={this.handleEndDateChange}
                              emptylabel="Select date"
                          />
                      </Grid>
                  </MuiPickersUtilsProvider>
              </Row>
              <Row className="form-group">
                  <FormControlLabel
                      control={
                          <Checkbox
                              checked={this.state.isBreakfastChecked}
                              onChange={this.handleCheckboxChange('isBreakfastChecked')}
                              value="isBreakfastChecked"
                          />
                      }
                      label="Breakfast"
                  />
                  <FormControlLabel
                      control={
                          <Checkbox
                              checked={this.state.isLunchChecked}
                              onChange={this.handleCheckboxChange('isLunchChecked')}
                              value="isLunchChecked"
                          />
                      }
                      label="Lunch"
                  />
                  <FormControlLabel
                      control={
                          <Checkbox
                              checked={this.state.isDinnerChecked}
                              onChange={this.handleCheckboxChange('isDinnerChecked')}
                              value="isDinnerChecked"
                          />
                      }
                      label="Dinner"
                  />
              </Row>
          </LocalForm>
      );
  }*/