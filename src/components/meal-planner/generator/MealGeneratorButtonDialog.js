import MealGeneratorForm from "./MealGeneratorForm";
import { Button, Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
export default class MealGeneratorButtonDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openMealGenerationDialog: false
        };
        this.handleCancel = this.handleCancel.bind(this)
        this.handleGenerate = this.handleGenerate.bind(this)
        this.handleOpen = this.handleOpen.bind(this)
    }

    handleOpen() {
        this.setState({ openMealGenerationDialog: true });
    };

    handleCancel() {
        this.setState({ openMealGenerationDialog: false });
    };

    handleGenerate(values) {
        this.setState({ openMealGenerationDialog: false });
        console.log(JSON.stringify(values))
    }


    render() {
        const MEAL_GENERATOR_FORM_INIT = { start_date: this.props.start_date, end_date: this.props.end_date, breakfast: true, lunch: true, dinner: true };
        return (
            <div>
                <Button variant="outlined" color="primary" onClick={this.handleOpen}>
                    <FontAwesomeIcon icon="calendar-alt" color="orange" />
                </Button>
                <Dialog open={this.state.openMealGenerationDialog} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Generate week meal planning</DialogTitle>
                    <DialogContent>
                        <MealGeneratorForm initialValues={MEAL_GENERATOR_FORM_INIT} onSubmit={this.handleGenerate} handleCancel={this.handleCancel} />
                    </DialogContent>
                </Dialog>
            </div>
        )
    }
}