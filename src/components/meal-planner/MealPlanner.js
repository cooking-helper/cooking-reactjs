import MealPlannerToolBar from "./toolbar/MealPlannerToolbar";
import MealPlannerNavBar from "./navbar/MealPlannerNavBar";
import MealPlannerCalendar from "./calendar/MealPlannerCalendar";
import "./MealPlanner.css";

import React from "react";
import { Container } from 'react-bootstrap';

export default class MealPlanner extends React.Component {
  constructor(props) {
    super(props);

    //Methods
    this.getStartDate = this.getStartDate.bind(this);
    this.getLastSunday = this.getLastSunday.bind(this);
    this.getPreviousWeek = this.getPreviousWeek.bind(this)
    this.getNextWeek = this.getNextWeek.bind(this)
    this.getEndDate = this.getEndDate.bind(this)

    //Init state
    const start_date = this.getStartDate();
    const end_date = this.getEndDate(start_date);
    this.state = {
      meal_planning_start_date: start_date,
      meal_planning_end_date: end_date,
    };
  }

  getStartDate() {
    const today = new Date();
    return this.getLastSunday(today);
  }

  getEndDate(d) {
    const days_to_add = 6;
    return new Date(d.getTime() + days_to_add * 86400000);
  }

  getLastSunday(d) {
    const temp = new Date(d);
    const day = temp.getDay();
    if (day === 0) {
      return temp;
    } else {
      const diff = temp.getDate() - day;
      return new Date(temp.getFullYear(), temp.getMonth(), diff);
    }
  }

  getPreviousWeek() {
    const temp = new Date(this.state.meal_planning_start_date);
    const diff = temp.getDate() - 7;
    const firstWeekDay = new Date(temp.getFullYear(), temp.getMonth(), diff);
    const lastWeekDay = this.getEndDate(firstWeekDay)
    this.setState({ meal_planning_start_date: firstWeekDay, meal_planning_end_date: lastWeekDay });
  }

  getNextWeek() {
    const temp = new Date(this.state.meal_planning_start_date);
    const diff = temp.getDate() + 7;
    const firstWeekDay = new Date(temp.getFullYear(), temp.getMonth(), diff);
    const lastWeekDay = this.getEndDate(firstWeekDay)
    this.setState({ meal_planning_start_date: firstWeekDay, meal_planning_end_date: lastWeekDay });
  }

  render() {
    return (
      <Container>
        <MealPlannerToolBar start_date={this.state.meal_planning_start_date} end_date={this.state.meal_planning_end_date} />
        <MealPlannerNavBar start_date={this.state.meal_planning_start_date} end_date={this.state.meal_planning_end_date} handleNext={this.getNextWeek} handlePrevious={this.getPreviousWeek} />
        <MealPlannerCalendar start_date={this.state.meal_planning_start_date} end_date={this.state.meal_planning_end_date} />
      </Container>
    );
  }
}