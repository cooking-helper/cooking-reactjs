import MealGeneratorButtonDialog from "../generator/MealGeneratorButtonDialog";
import './MealPlannerToolbar.css';
import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
export default class MealPlannerToolbar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div align="right" className="row justify-content-end">
                    <div className="icon-toolbar-item" >
                        <MealGeneratorButtonDialog start_date={this.props.start_date} end_date={this.props.end_date}></MealGeneratorButtonDialog>
                    </div>
                    <div className="icon-toolbar-item" >
                        <a href="#">
                            <FontAwesomeIcon icon="shopping-cart" color="orange" />
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
/*export default function MealPlannerToolbar() {
    return (
        <div>
            <div align="right" className="row justify-content-end">
                <div className="icon-toolbar-item" >
                    <MealGeneratorButtonDialog start_date={this.props.start_date}></MealGeneratorButtonDialog>
                </div>
                <div className="icon-toolbar-item" >
                    <a href="#">
                        <FontAwesomeIcon icon="shopping-cart" color="orange" />
                    </a>
                </div>
            </div>
        </div>
    )
}*/