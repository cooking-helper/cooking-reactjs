
import { connect } from 'react-redux'
import { Table } from 'react-bootstrap';

const mapStateToProps = store => {
    return {
        meal_planning: store.meal_planning
    }
}

class MealPlannerCalendar extends React.Component {
    constructor(props) {
        super(props);
        this.formatMealPlanning = this.formatMealPlanning.bind(this);
    }


    formatMealPlanning(meal_planning) {
        const meal_planning_columns_header = meal_planning.map((day_planning) => day_planning.day_of_week);
        meal_planning_columns_header.unshift("");

        const meal_planning_rows = [];
        const meal_planning_breakfast_row = meal_planning.map((day_planning) => { if (day_planning.meals === null || day_planning.meals.BREAKFAST == null) { return "" } else return (day_planning.meals.BREAKFAST) });
        meal_planning_breakfast_row.unshift("Breakfast");
        const meal_planning_lunch_row = meal_planning.map((day_planning) => { if (day_planning.meals === null || day_planning.meals.LUNCH == null) { return "" } else return (day_planning.meals.LUNCH) });
        meal_planning_lunch_row.unshift("Lunch");
        const meal_planning_dinner_row = meal_planning.map((day_planning) => { if (day_planning.meals === null || day_planning.meals.DINNER == null) { return "" } else return (day_planning.meals.DINNER) });
        meal_planning_dinner_row.unshift("Dinner");
        meal_planning_rows.push(meal_planning_breakfast_row);
        meal_planning_rows.push(meal_planning_lunch_row);
        meal_planning_rows.push(meal_planning_dinner_row);

        return [meal_planning_columns_header, meal_planning_rows];
    }

    render() {
        const [meal_planning_columns_header, meal_planning_rows] = this.formatMealPlanning(this.props.meal_planning);
        const header_items = meal_planning_columns_header.map((day_of_week) => <th key={day_of_week}>{day_of_week}</th>)
        const row_items = meal_planning_rows.map((row, i) => (<tr key={i}> {row.map((cell, i) => (<td key={i}>{cell}</td>))}</tr>))
        return (
            <Table className="calendar-week" striped bordered hover>
                <thead>
                    <tr>
                        {header_items}
                    </tr>
                </thead>
                <tbody>
                    {row_items}
                </tbody>
            </Table>
        )
    }
} export default (connect(mapStateToProps))(MealPlannerCalendar)
