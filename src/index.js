import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import CookingPlannerLayout from "./components/index-layout/CookingPlannerLayout";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faShoppingCart, faCalendarAlt, faHamburger } from '@fortawesome/free-solid-svg-icons'
import { Provider } from 'react-redux'
import { ConfigureStore } from './redux/configureStore'

library.add(faShoppingCart)
library.add(faCalendarAlt)
library.add(faHamburger)

const app = document.getElementById('index');
const store = ConfigureStore();
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <CookingPlannerLayout />
        </BrowserRouter>
    </Provider>,
    app);